Readme File for my JFLEX Scanner Project 4
Last Updated: Thursday, 4/30/2020 
Author: Shamarr McKinney-VanBuren 
Latest version available: bitbucket.org/shamarrmckinney/mckinneyvanburenjflexproject4/src 

== Overview ==

This program uses JFlex metalanguage to create a functioning scanner.

Comprised of flex files that produces a standalone scanner which can properly 

Recognize numbers and words.

This program hs a Token class, a TokenType class, and a Main class, and a LookupTable class

Notable Features: Returns Tokens, Error Detection, Ignores comments.

Source code found in src folder in above bit bucket repository. 
 

=======================================================================